#!/usr/bin/python

import sys

if (len(sys.argv) == 2):
    js_code = sys.argv[1]
    print '<script>eval(String.fromCharCode(' + ','.join([str(ord(char)) for char in js_code]) + '))</script>'
